package cz.vse.java.xvalm00.adventuracv.main;

import cz.vse.java.xvalm00.adventuracv.logika.HerniPlan;
import cz.vse.java.xvalm00.adventuracv.logika.Prostor;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ListView;

public class PanelVychodu {

    private final HerniPlan plan;
    ListView<String> listView = new ListView<>();
    ObservableList<String> vychody = FXCollections.observableArrayList();

    public PanelVychodu(HerniPlan plan) {
        this.plan = plan;
        init();
    }

    private void init() {
        listView.setItems(vychody);
        listView.setPrefWidth(100);
        for (Prostor vychod : plan.getAktualniProstor().getVychody()) {
            vychody.add(vychod.getNazev());
        }
    }

    public ListView<String> getListView(){
        return listView;
    }

}
